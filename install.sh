#!/usr/bin/env bash

#************************************************************
#
# Installation of Traefik / Crowdsec / Letsencrypt / Appstack
# execute as root
#
#************************************************************

# see: https://goneuland.de/traefik-v2-3-reverse-proxy-mit-crowdsec-im-stack-einrichten/

export DEBIAN_FRONTEND=noninteractive

# for letsencrypt
STACK_DOMAIN="appstack.works"
STACK_EMAIL="eqsoft4@gmail.com"

# maybe additional collections?
CROWDSEC_COLLECTIONS="crowdsecurity\/traefik crowdsecurity\/http-cve crowdsecurity\/whitelist-good-actors crowdsecurity\/nginx"

# traefik dasboard basic auth access
TRAEFIK_BASIC_AUTH_PASSWORD="YOUR_BASIC_AUTH_PASSWORD"

# appstack version / take a look for newer version url
APPSTACK_URL="https://gitlab.com/eqsoft-appstack/appsctl/-/package_files/89195027/download"

install_packages () {
	DEBIAN_FRONTEND=noninteractive apt update
	apt install -y -qq \
		apt-transport-https \
		ca-certificates \
		curl \
		software-properties-common \
		unattended-upgrades \
		apache2-utils \
    uidmap \
		tree
  sysctl -w net.ipv6.conf.all.disable_ipv6=1
  sysctl -w net.ipv4.ip_unprivileged_port_start=0
}

install_crowdsec_firewall_bouncer () {
	curl -s https://packagecloud.io/install/repositories/crowdsec/crowdsec/script.deb.sh | bash
	apt update
	DEBIAN_FRONTEND=noninteractive apt install -y -qq crowdsec-firewall-bouncer-iptables
}

install_and_start_docker () {
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
	echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
	apt update
	apt-cache policy docker-ce
	apt update
	DEBIAN_FRONTEND=noninteractive apt install -y -qq docker-ce
	DEBIAN_FRONTEND=noninteractive apt install -y -qq docker-compose
	# required:
	# systemctl enable docker
	# activate userns-remapping - https://docs.docker.com/engine/security/userns-remap/
	# reduce networks size to /24
	cat << 'EOF' > /etc/docker/daemon.json
{
  "userns-remap": "default",
  "default-address-pools" : [
    {
      "base" : "172.17.0.0/16",
      "size" : 24
    }
  ]
}
EOF
  
  cat << 'EOF' >> ~/.bashrc
alias d='docker'
alias d-c='docker-compose'
alias lisa='ls -lat --color=auto'
alias ipl='iptables -L -n -v'
EOF
	systemctl daemon-reload
  systemctl restart docker
}

create_filesystem () {
	# Hauptverzeichnis erstellen und zusätzliche Unterordner in 'crowdsec' erstellen
	mkdir -p /opt/containers/traefik-crowdsec-stack/{traefik,crowdsec/{config,data},config}

	# Ins Hauptverzeichnis wechseln
	cd /opt/containers/traefik-crowdsec-stack

	# .env Datei im Hauptverzeichnis erstellen
	touch /opt/containers/traefik-crowdsec-stack/.env

	# Umgebungsspezifische .env Dateien in 'config' erstellen
	touch /opt/containers/traefik-crowdsec-stack/config/{crowdsec.env,traefik.env,traefik-crowdsec-bouncer.env}

	# Zusätzliche Dateien in 'traefik' erstellen und Zugriffsrechte für bestimmte Dateien festlegen
	touch /opt/containers/traefik-crowdsec-stack/traefik/{acme_letsencrypt.json,traefik.yml,dynamic_conf.yml,tls_letsencrypt.json}
	chmod 600 /opt/containers/traefik-crowdsec-stack/traefik/{acme_letsencrypt.json,tls_letsencrypt.json}
	tree -L 2 -a /opt/containers/traefik-crowdsec-stack/
}

create_docker_compose () {
	cd /opt/containers/traefik-crowdsec-stack
	cat << 'EOF' > ./docker-compose.yaml
version: "3.9"
services:
  crowdsec:
    userns_mode: host
    container_name: ${SERVICES_CROWDSEC_CONTAINER_NAME:-crowdsec}
    env_file: ./config/crowdsec.env
    hostname: ${SERVICES_CROWDSEC_HOSTNAME:-crowdsec}
    healthcheck:
      test: ["CMD", "cscli", "version"]
      interval: 20s
      timeout: 2s
      retries: 5
      start_period: 10s
    image: ${SERVICES_CROWDSEC_IMAGE:-crowdsecurity/crowdsec}:${SERVICES_CROWDSEC_IMAGE_VERSION:-latest}
    networks:
      crowdsec:
        ipv4_address: ${SERVICES_CROWDSEC_NETWORKS_CROWDSEC_IPV4:-172.31.254.254}
    restart: unless-stopped
    security_opt:
      - no-new-privileges=true
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - /var/log/auth.log:/var/log/auth.log:ro
      - /var/log/traefik:/var/log/traefik:ro
      - ./crowdsec/config:/etc/crowdsec
      - ./crowdsec/data:/var/lib/crowdsec/data
  traefik:
    userns_mode: host
    container_name: ${SERVICES_TRAEFIK_CONTAINER_NAME:-traefik}
    depends_on:
      crowdsec:
        condition: service_healthy
    env_file: ./config/traefik.env
    hostname: ${SERVICES_TRAEFIK_HOSTNAME:-traefik}
    healthcheck:
      test: ["CMD", "traefik", "healthcheck", "--ping"]
      interval: 10s
      timeout: 1s
      retries: 3
      start_period: 10s
    image: ${SERVICES_TRAEFIK_IMAGE:-traefik}:${SERVICES_TRAEFIK_IMAGE_VERSION:-2.10}
    labels:
      traefik.docker.network: proxy
      traefik.enable: "true"
      traefik.http.routers.traefik.entrypoints: websecure
      traefik.http.routers.traefik.middlewares: default@file,traefikAuth@file
      traefik.http.routers.traefik.rule: Host(`${SERVICES_TRAEFIK_LABELS_TRAEFIK_HOST}`)
      traefik.http.routers.traefik.service: api@internal
      traefik.http.routers.traefik.tls: "true"
      traefik.http.routers.traefik.tls.certresolver: http_resolver
      #traefik.http.routers.blog.tls.domains[0].main: "${STACK_DOMAIN}"
      #traefik.http.routers.blog.tls.domains[0].sans: "*.${STACK_DOMAIN}"
      traefik.http.services.traefik.loadbalancer.sticky.cookie.httpOnly: "true"
      traefik.http.services.traefik.loadbalancer.sticky.cookie.secure: "true"
      traefik.http.routers.pingweb.rule: PathPrefix(`/ping`)
      traefik.http.routers.pingweb.service: ping@internal
      traefik.http.routers.pingweb.entrypoints: websecure
    networks:
      crowdsec:
        ipv4_address: ${SERVICES_TRAEFIK_NETWORKS_CROWDSEC_IPV4:-172.31.254.253}
      proxy:
        ipv4_address: ${SERVICES_TRAEFIK_NETWORKS_PROXY_IPV4:-172.30.255.254}
    ports:
      - "80:80"
      - "443:443"
    restart: unless-stopped
    security_opt:
      - no-new-privileges:true
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - /var/log/traefik/:/var/log/traefik/
      - ./traefik/traefik.yml:/traefik.yml:ro
      - ./traefik/acme_letsencrypt.json:/acme_letsencrypt.json
      - ./traefik/tls_letsencrypt.json:/tls_letsencrypt.json
      - ./traefik/dynamic_conf.yml:/dynamic_conf.yml
  traefik_crowdsec_bouncer:
    container_name: ${SERVICES_TRAEFIK_CROWDSEC_BOUNCER_CONTAINER_NAME:-traefik_crowdsec_bouncer}
    depends_on:
      crowdsec:
        condition: service_healthy
    env_file: ./config/traefik-crowdsec-bouncer.env
    hostname: ${SERVICES_TRAEFIK_CROWDSEC_BOUNCER_HOSTNAME:-traefik-crowdsec-bouncer}
    image: ${SERVICES_TRAEFIK_CROWDSEC_BOUNCER_IMAGE:-fbonalair/traefik-crowdsec-bouncer}:${SERVICES_TRAEFIK_CROWDSEC_BOUNCER_IMAGE_VERSION:-latest}
    networks:
      crowdsec:
        ipv4_address: ${SERVICES_TRAEFIK_CROWDSEC_BOUNCER_NETWORKS_CROWDSEC_IPV4:-172.31.254.252}
    restart: unless-stopped
networks:
  proxy:
    name: ${NETWORKS_PROXY_NAME:-proxy}
    driver: bridge
    ipam:
      config:
      - subnet: ${NETWORKS_PROXY_SUBNET_IPV4:-172.30.0.0/16}
    attachable: true
  crowdsec:
    name: ${NETWORKS_CROWDSEC_NAME:-crowdsec}
    driver: bridge
    ipam:
      config:
      - subnet: ${NETWORKS_CROWDSEC_SUBNET_IPV4:-172.31.0.0/16}
    attachable: true
EOF
}

create_env_file () {
  cd /opt/containers/traefik-crowdsec-stack
  cat << 'EOF' > ./.env
# global
STACK_DOMAIN=MY_STACK_DOMAIN
# Service Crowdsec
SERVICES_CROWDSEC_CONTAINER_NAME=crowdsec
SERVICES_CROWDSEC_HOSTNAME=crowdsec
SERVICES_CROWDSEC_IMAGE=crowdsecurity/crowdsec
SERVICES_CROWDSEC_IMAGE_VERSION=latest
SERVICES_CROWDSEC_NETWORKS_CROWDSEC_IPV4=172.31.254.254
# Service Traefik
SERVICES_TRAEFIK_CONTAINER_NAME=traefik
SERVICES_TRAEFIK_HOSTNAME=router
SERVICES_TRAEFIK_IMAGE=traefik
SERVICES_TRAEFIK_IMAGE_VERSION=2.10
SERVICES_TRAEFIK_LABELS_TRAEFIK_HOST=router.MY_STACK_DOMAIN
SERVICES_TRAEFIK_NETWORKS_CROWDSEC_IPV4=172.31.254.253
SERVICES_TRAEFIK_NETWORKS_PROXY_IPV4=172.30.255.254
# Service Traefik Crowdsec Bouncer
SERVICES_TRAEFIK_CROWDSEC_BOUNCER_CONTAINER_NAME=traefik_crowdsec_bouncer
SERVICES_TRAEFIK_CROWDSEC_BOUNCER_HOSTNAME=traefik-crowdsec-bouncer
SERVICES_TRAEFIK_CROWDSEC_BOUNCER_IMAGE=fbonalair/traefik-crowdsec-bouncer
SERVICES_TRAEFIK_CROWDSEC_BOUNCER_IMAGE_VERSION=latest
SERVICES_TRAEFIK_CROWDSEC_BOUNCER_NETWORKS_CROWDSEC_IPV4=172.31.254.252
# Netzwerkeinstellungen
NETWORKS_PROXY_NAME=proxy
NETWORKS_PROXY_SUBNET_IPV4=172.30.0.0/16
NETWORKS_CROWDSEC_NAME=crowdsec
NETWORKS_CROWDSEC_SUBNET_IPV4=172.31.0.0/16
EOF

	sed -i "s/MY_STACK_DOMAIN/${STACK_DOMAIN}/g" ./.env
}

create_traefik_yaml () {
	cd /opt/containers/traefik-crowdsec-stack/traefik/
cat << 'EOF' > traefik.yml
api:
  dashboard: true

metrics:
  prometheus:
    addrouterslabels: true

certificatesResolvers:
  http_resolver:
    acme:
      email: "MY_STACK_EMAIL"
      storage: "acme_letsencrypt.json"
      httpChallenge:
        entryPoint: web
  tls_resolver:
    acme:
      tlsChallenge: true
      email: "MY_STACK_EMAIL"
      storage: "tls_letsencrypt.json"
entryPoints:
  ping:
    address: ":88"
  web:
    address: ":80"
    http:
      redirections:
        entryPoint:
          to: "websecure"
          scheme: "https"
      middlewares:
        - traefik-crowdsec-bouncer@file
  websecure:
    address: ":443"
    http:
      middlewares:
        - traefik-crowdsec-bouncer@file
    proxyProtocol:
      trustedIPs:
       - 10.0.0.0/8
       - 172.16.0.0/12
       - 192.168.0.0/16
    forwardedHeaders:
      trustedIPs:
       - 10.0.0.0/8
       - 172.16.0.0/12
       - 192.168.0.0/16

ping:
  entryPoint: "ping"

global:
  checknewversion: true
  sendanonymoususage: false

experimental:
  plugins:
    real-ip:
      moduleName: github.com/Paxxs/traefik-get-real-ip
      version: "v1.0.2"

providers:
  docker:
    endpoint: "unix:///var/run/docker.sock"
    exposedByDefault: false
    network: "proxy"
  file:
    filename: "./dynamic_conf.yml"
    watch: true
  providersThrottleDuration: 10

log:
  level: "INFO"
  filePath: "/var/log/traefik/traefik.log"
accessLog:
  filePath: "/var/log/traefik/access.log"
  bufferingSize: 100
EOF
	sed -i "s/MY_STACK_EMAIL/${STACK_EMAIL}/g" ./traefik.yml
}

create_dynamic_conf_yaml () {
	cd /opt/containers/traefik-crowdsec-stack/traefik/
	cat << 'EOF' > ./dynamic_conf.yml
tls:
  options:
    default:
      minVersion: VersionTLS12
      cipherSuites:
        - TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
        - TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384
        - TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305
        - TLS_AES_128_GCM_SHA256
        - TLS_AES_256_GCM_SHA384
        - TLS_CHACHA20_POLY1305_SHA256
      curvePreferences:
        - CurveP521
        - CurveP384
      sniStrict: true
http:
  middlewares:
    default:
      chain:
        middlewares:
          - default-security-headers
          - gzip

    default-security-headers:
      headers:
        browserXssFilter: true
        contentTypeNosniff: true
        forceSTSHeader: true
        frameDeny: true
        stsIncludeSubdomains: true
        stsPreload: true
        stsSeconds: 31536000
        customFrameOptionsValue: "SAMEORIGIN"

    gzip:
      compress: {}

    traefik-crowdsec-bouncer:
      forwardauth:
        address: http://traefik-crowdsec-bouncer:8080/api/v1/forwardAuth
        trustForwardHeader: true

    real-ip-cf:
      plugin:
        real-ip:
          Proxy:
            - proxyHeadername: "*"
              realIP: Cf-Connecting-Ip
              OverwriteXFF: true
    traefikAuth:
      basicAuth:
        users:
          - "MY_BASIC_AUTH"
EOF
	BASIC_AUTH=$(htpasswd -nb admin $TRAEFIK_BASIC_AUTH_PASSWORD)
	sed -i "s/MY_BASIC_AUTH/${BASIC_AUTH}/g" ./dynamic_conf.yml
}

create_crowdsec_env () {
	cd /opt/containers/traefik-crowdsec-stack/config/
 	cat << 'EOF' > ./crowdsec.env
PGID="0"
COLLECTIONS="MY_CROWDSEC_COLLECTIONS"
EOF
	sed -i "s/MY_CROWDSEC_COLLECTIONS/$CROWDSEC_COLLECTIONS/g" ./crowdsec.env
}

create_acquis_yaml () {
	cd /opt/containers/traefik-crowdsec-stack
	docker compose up crowdsec -d && docker compose down
	cat << 'EOF' >> ./crowdsec/config/acquis.yaml
---
filenames:
  - /var/log/traefik/*.log
labels:
  type: traefik
EOF
}

create_traefik_crowdsec_bouncer_api_key () {
	cd /opt/containers/traefik-crowdsec-stack
	. .env
	docker compose up crowdsec -d
	API_KEY=$(docker compose exec -t crowdsec cscli bouncers add traefik-crowdsec-bouncer -o raw)
	docker compose down
	echo ${API_KEY}
	cat << 'EOF' > ./config/traefik-crowdsec-bouncer.env
# Access-Token damit Bouncer und CrowdSec kommunizieren können
CROWDSEC_BOUNCER_API_KEY=MY_API_KEY
# Hostname mit richtigem Port von CrowdSec
CROWDSEC_AGENT_HOST=MY_SERVICES_CROWDSEC_HOSTNAME:8080
EOF
	sed -i "s/MY_API_KEY/$API_KEY/g" ./config/traefik-crowdsec-bouncer.env
	sed -i "s/MY_SERVICES_CROWDSEC_HOSTNAME/$SERVICES_CROWDSEC_HOSTNAME/g" ./config/traefik-crowdsec-bouncer.env
}

create_firewall_crowdsec_bouncer_api_key () {
	cd /opt/containers/traefik-crowdsec-stack
  . .env
	CONFIG_FILE="/etc/crowdsec/bouncers/crowdsec-firewall-bouncer.yaml"
  docker compose up crowdsec -d
  API_KEY=$(docker compose exec -t crowdsec cscli bouncers add crowdsec-firewall-bouncer-iptables -o raw)
  docker compose down
	sed -i "s/api_url\:.*/api_url\: http:\/\/$SERVICES_CROWDSEC_NETWORKS_CROWDSEC_IPV4\:8080\//" "${CONFIG_FILE}"
	sed -i "s/api_key\:.*/api_key\: $API_KEY/" "${CONFIG_FILE}"
	sed -i "s/disable_ipv6\:.*/disable_ipv6\: true/" "${CONFIG_FILE}"
	sed -i "s/.*FORWARD.*/  - FORWARD/" "${CONFIG_FILE}"
	sed -i "s/.*DOCKER-USER.*/  - DOCKER-USER/" "${CONFIG_FILE}"
	systemctl enable crowdsec-firewall-bouncer
}

restart_stack () {
	cd /opt/containers/traefik-crowdsec-stack
  docker compose down
	docker compose up -d --force-recreate
	sleep 60
	systemctl restart crowdsec-firewall-bouncer
}

create_traefik_logrotate () {
  . .env
  LOGROTATE_FILE="/etc/logrotate.d/traefik"
  cat << 'EOF' > $LOGROTATE_FILE 
  /var/log/traefik/access.log {
  size 10M
  rotate 5
  missingok
  notifempty
  postrotate
    docker kill --signal="USR1" CONTAINER_NAME
  endscript
}
EOF
  sed -i "s/CONTAINER_NAME/$SERVICES_TRAEFIK_CONTAINER_NAME/g" $LOGROTATE_FILE
  chown 100000:root /var/log/traefik
  chmod 775 /var/log/traefik
}

install_appstack () {
  mkdir ~/.config/appsctl
  curl -o /usr/local/bin/appsctl "$APPSTACK_URL"
  chmod 755 /usr/local/bin/appsctl
  if [ -f "./config.yaml" ] ; then
    mv ./config.yaml ~/.config/appsctl/
    /usr/local/bin/./appsctl config
    /usr/local/bin/./appsctl start
  else
    echo "no initial appstack config"
  fi
}

install_packages
install_crowdsec_firewall_bouncer
install_and_start_docker
create_filesystem
create_docker_compose
create_env_file
create_traefik_yaml
create_dynamic_conf_yaml
create_crowdsec_env
create_acquis_yaml
create_traefik_crowdsec_bouncer_api_key
create_firewall_crowdsec_bouncer_api_key
restart_stack
create_traefik_logrotate
install_appstack
# sync ssh_keys into setup of sshd launcher